import {Component, Input, OnInit} from '@angular/core';
import {UserPostDetails} from '../../../domain/user-post-details.class';

@Component({
  selector: 'app-user-post-details-video',
  templateUrl: './user-post-details-video.component.html',
  styleUrls: ['./user-post-details-video.component.scss']
})
export class UserPostDetailsVideoComponent implements OnInit {

  constructor() { }

  @Input() data: UserPostDetails;

  ngOnInit() {
  }

}
