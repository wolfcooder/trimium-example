import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPostDetailsVideoComponent } from './user-post-details-video.component';

describe('UserPostDetailsVideoComponent', () => {
  let component: UserPostDetailsVideoComponent;
  let fixture: ComponentFixture<UserPostDetailsVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPostDetailsVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPostDetailsVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
