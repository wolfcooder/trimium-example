import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {UserPostDetails} from '../../domain/user-post-details.class';
import {ApiService} from '../../services/api/api.service';
import {Observable} from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserPostDetailsResolver implements Resolve<any> {
  constructor(private api: ApiService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<UserPostDetails> {
    const id = route.paramMap.get('id');
    return this.api.get<UserPostDetails>('posts/' + id);
  }
}
