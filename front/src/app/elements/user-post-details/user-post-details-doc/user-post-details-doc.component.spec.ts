import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPostDetailsDocComponent } from './user-post-details-doc.component';

describe('UserPostDetailsDocComponent', () => {
  let component: UserPostDetailsDocComponent;
  let fixture: ComponentFixture<UserPostDetailsDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPostDetailsDocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPostDetailsDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
