import {Component, Input, OnInit} from '@angular/core';
import {UserPostDetails} from '../../../domain/user-post-details.class';

@Component({
  selector: 'app-user-post-details-doc',
  templateUrl: './user-post-details-doc.component.html',
  styleUrls: ['./user-post-details-doc.component.scss']
})
export class UserPostDetailsDocComponent implements OnInit {

  constructor() { }

  @Input() data: UserPostDetails;


  ngOnInit() {
  }

}
