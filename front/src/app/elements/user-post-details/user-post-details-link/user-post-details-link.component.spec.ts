import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPostDetailsLinkComponent } from './user-post-details-link.component';

describe('UserPostDetailsLinkComponent', () => {
  let component: UserPostDetailsLinkComponent;
  let fixture: ComponentFixture<UserPostDetailsLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPostDetailsLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPostDetailsLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
