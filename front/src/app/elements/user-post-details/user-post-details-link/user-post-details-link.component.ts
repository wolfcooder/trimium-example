import {Component, Input, OnInit} from '@angular/core';
import {UserPostDetails} from '../../../domain/user-post-details.class';

@Component({
  selector: 'app-user-post-details-link',
  templateUrl: './user-post-details-link.component.html',
  styleUrls: ['./user-post-details-link.component.scss']
})
export class UserPostDetailsLinkComponent implements OnInit {

  constructor() { }

  @Input() data: UserPostDetails;


  ngOnInit() {
  }

}
