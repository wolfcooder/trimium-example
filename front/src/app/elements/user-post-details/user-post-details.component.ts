import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Resolve, Router} from '@angular/router';
import {ApiService} from '../../services/api/api.service';
import {UserPostDetails} from '../../domain/user-post-details.class';

@Component({
  selector: 'app-user-post-details',
  templateUrl: './user-post-details.component.html',
  styleUrls: ['./user-post-details.component.scss']
})
export class UserPostDetailsComponent implements OnInit {

  @Input() data: UserPostDetails;


  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(({details}) => {
      this.data = details;
    });

  }

}
