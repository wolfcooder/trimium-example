import {Component, Input, OnInit} from '@angular/core';
import {UserPostDetails} from '../../../domain/user-post-details.class';

@Component({
  selector: 'app-user-post-details-photo',
  templateUrl: './user-post-details-photo.component.html',
  styleUrls: ['./user-post-details-photo.component.scss']
})
export class UserPostDetailsPhotoComponent implements OnInit {

  constructor() { }

  @Input() data: UserPostDetails;


  ngOnInit() {
  }

}
