import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPostDetailsPhotoComponent } from './user-post-details-photo.component';

describe('UserPostDetailsPhotoComponent', () => {
  let component: UserPostDetailsPhotoComponent;
  let fixture: ComponentFixture<UserPostDetailsPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPostDetailsPhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPostDetailsPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
