import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-navigation-menu-item',
  templateUrl: './navigation-menu-item.component.html',
  styleUrls: ['./navigation-menu-item.component.scss']
})
export class NavigationMenuItemComponent implements OnInit {

  constructor() { }

  @Input() routerLink: string;
  @Input() displayText: string;

  ngOnInit() {
  }

}
