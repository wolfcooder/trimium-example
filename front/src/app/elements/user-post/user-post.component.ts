import {Component, Input, OnInit} from '@angular/core';
import {UserPost} from '../../domain/user-post.class';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ApiService} from '../../services/api/api.service';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-user-post',
  templateUrl: './user-post.component.html',
  styleUrls: ['./user-post.component.scss']
})
export class UserPostComponent implements OnInit {

  @Input() data: UserPost;

  constructor() { }

  ngOnInit() {
  }

}
