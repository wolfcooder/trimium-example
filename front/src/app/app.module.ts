import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './layouts/main/main.component';
import { HeaderComponent } from './blocks/header/header.component';
import { NavigationMenuComponent } from './blocks/navigation-menu/navigation-menu.component';
import { NavigationMenuItemComponent } from './elements/navigation-menu-item/navigation-menu-item.component';
import { NotFoundComponent } from './layouts/content/not-found/not-found.component';
import { SocialNetworkComponent } from './layouts/content/social-network/social-network.component';
import { UserPostComponent } from './elements/user-post/user-post.component';
import {ApiService} from './services/api/api.service';
import {HttpClientModule} from '@angular/common/http';
import { UserPostDetailsComponent } from './elements/user-post-details/user-post-details.component';
import { UserPostDetailsPhotoComponent } from './elements/user-post-details/user-post-details-photo/user-post-details-photo.component';
import { UserPostDetailsVideoComponent } from './elements/user-post-details/user-post-details-video/user-post-details-video.component';
import { UserPostDetailsLinkComponent } from './elements/user-post-details/user-post-details-link/user-post-details-link.component';
import { UserPostDetailsDocComponent } from './elements/user-post-details/user-post-details-doc/user-post-details-doc.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatVideoModule} from 'mat-video';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HeaderComponent,
    NavigationMenuComponent,
    NavigationMenuItemComponent,
    NotFoundComponent,
    SocialNetworkComponent,
    UserPostComponent,
    UserPostDetailsComponent,
    UserPostDetailsPhotoComponent,
    UserPostDetailsVideoComponent,
    UserPostDetailsLinkComponent,
    UserPostDetailsDocComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatVideoModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
