import { Component, OnInit } from '@angular/core';
import {UserPost} from '../../../domain/user-post.class';
import {ApiService} from '../../../services/api/api.service';

@Component({
  selector: 'app-social-network',
  templateUrl: './social-network.component.html',
  styleUrls: ['./social-network.component.scss']
})
export class SocialNetworkComponent implements OnInit {

  userPosts: Array<UserPost> = [];

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.get<Array<UserPost>>('posts').subscribe(
      (data) => this.userPosts = data.map(
        userPost => new UserPost(userPost)
      )
    );
  }

}
