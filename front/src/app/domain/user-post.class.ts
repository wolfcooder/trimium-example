export class UserPost{
  public id: number;
  public author: string;
  public text: string;

  public constructor(data: any){
    return Object.assign<UserPost, any>(this, data);
  }
}
