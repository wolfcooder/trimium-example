export class UserPostDetails {
  public id: number;
  public author: string;
  public text: string;
  public type: string;
  public content: string;

  public constructor(data: any) {
    return Object.assign<UserPostDetails, any>(this, data);
  }
}
