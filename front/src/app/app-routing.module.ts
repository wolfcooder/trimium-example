import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotFoundComponent} from './layouts/content/not-found/not-found.component';
import {MainComponent} from './layouts/main/main.component';
import {SocialNetworkComponent} from './layouts/content/social-network/social-network.component';
import {UserPostDetailsComponent} from './elements/user-post-details/user-post-details.component';
import {UserPostDetailsResolver} from './elements/user-post-details/user-post-details.resolver';

const routes: Routes = [

      {path: '', component: MainComponent, children: [
      {path: 'social-network', component: SocialNetworkComponent},
      {path: 'not-found', component: NotFoundComponent},
      {path: 'post/:id', component: UserPostDetailsComponent, resolve: {details: UserPostDetailsResolver}},
    ]},

  {path: '', redirectTo: '/main', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
