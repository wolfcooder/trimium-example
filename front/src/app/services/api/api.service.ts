import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import * as ApiConfig from '../../../api.config.json';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {



  apiUrl = ApiConfig.apiUrl;

  constructor(private http: HttpClient) { }


  get<T>(endPoint, options = {}): Observable<T> {
      const headers = new HttpHeaders();
      return this.http.get<T>(this.apiUrl + endPoint, {params: options, headers});
  }

  post<T>(endPoint, options = {}): Observable<T> {
      const headers = new HttpHeaders();
      return this.http.post<T>(this.apiUrl + endPoint, options, {headers});
  }

  endpointUndefined(name) {
    console.log('Endpoit <' + name + '> undefined!');
  }

}
