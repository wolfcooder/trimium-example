// import {SignInService} from '../sign/sign-in.service'; //Циркулярная зависимость. Подумать как убрать

export class GlobalErrorsObserver {

    private static globalErrorCodes = [500, 503, 0];

    public static observe(code: number, message: string) {
        // if (code === 401) { SignInService.forceLogout(); return; }
        if (GlobalErrorsObserver.globalErrorCodes.includes(code)) { alert (message); }
    }
}
