const setHeaders = require("./headers");

module.exports = (app, pool) => {
    app.get("/comments/:id/", (req, res) => {
        setHeaders(res);

        pool.query("SELECT * FROM comments WHERE id = ? LEFT JOIN users ON comments.author_id = users.id", [req.params.id],
            (err, data) => {
                const result = data ? data[0] : {};
                res.json(result);
            }
        )
    });

    app.get("/comments/:id/comments", (req, res) => {
        setHeaders(res);

        pool.query("SELECT * FROM comments WHERE paerent_type = 'comment' AND paerent_id = ?", [req.params.id],
            (err, data) => {
                const result = data.map((item => item)); //TODO: преобразование к нужному классу
                res.json(result);
            })
    })
};
