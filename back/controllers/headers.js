module.exports = (res) => {
    const frontendHost = 'http://localhost:4200';
    res.setHeader('Access-Control-Allow-Origin', frontendHost);
};
