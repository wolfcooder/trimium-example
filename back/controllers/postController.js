const setHeaders = require("./headers");

module.exports = (app, pool) => {

    app.get("/posts", (req, res) => {
        setHeaders(res);

        pool.query("SELECT posts.*, users.name FROM posts LEFT JOIN users ON posts.author_id = users.id",
            (err, data) => {
                const result = data ? data.map((item) => {
                    item.author = item.name;
                    return item;
                }) : []; //TODO: преобразование к нужному классу
                res.json(result);
            });
    });

    app.get("/posts/:id", (req, res, next) => {
        setHeaders(res);

        pool.query("SELECT posts.*, users.name FROM posts LEFT JOIN users ON posts.author_id = users.id WHERE posts.id = ?", [req.params.id],
            (err, data) => {
                const result = data ? data[0] : {};
                result.author = result.name;
                res.json(result);
            });
    });

    app.get("/posts/:id/like", (req, res) => {
        setHeaders(res);

        pool.query("INSERT INTO likes SET paerent_id = ?, user_id = ?, paerent_type = 'post'", [req.params.id, req.query.user_id]);
        pool.query("UPDATE posts SET likes_count = likes_count + 1 WHERE id = ?", [req.params.id],
            (err, result) => {
                res.json(result);
            });

    });

    app.post("posts/:id/comment", (req, res) => {
        setHeaders(res);

        pool.query("INSERT INTO comments SET paerent_id = ?, author_id = ?, paerent_type = 'post', content = ?", [req.params.id, req.body.user_id, req.body.content]);
        pool.query("UPDATE posts SET comments_count = comments_count + 1 WHERE id = ?", [req.params.id],
            (err, result) => {
                res.json(result);
            }
            );
    });

    app.get("posts/:id/comments", (req, res) => {
        setHeaders(res);

        pool.query("SELECT * FROM comments LEFT JOIN users ON comments.author_id = users.id WHERE comments.paerent_type = 'post' AND paerent_id = ?", [req.params.id],
            (err, data) => {
                const result = data ? data.map((item) => item) : []; //TODO: преобразование к нужному классу
                res.json(result);
            })
    })

};
