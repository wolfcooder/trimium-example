const setHeaders = require("./headers");

module.exports = (app, pool) => {
    app.get("/users", (req, res) => {
        setHeaders(res);

        let sql = "SELECT * FROM users WHERE 1";

        if(req.query.order_by) {
            const order_type = req.query.order_type === 'desc' ? 'DESC' : 'ASC';
            sql += " ORDER BY " + req.query.order_by + " " + order_type;
        }

        if(req.query.limit) {
            const page = req.query.page ? req.query.page : 1;
            const offset = (page - 1) * req.query.limit;
            sql += " LIMIT " + req.query.limit;
            if(offset > 0) sql += " OFFSET " + offset;
        }

        console.log(sql);

        pool.query(sql,
            (err, data) => {
                const result = data ? data.map((item) => item) : [];
                res.json(result);
            });
    });

}
