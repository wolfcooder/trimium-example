const express = require('express');
const app = express();

const pool = require('./db/pool');

const initUserController = require('./controllers/userController');
const initPostController = require('./controllers/postController');
const initCommentController = require('./controllers/commentController');

initUserController(app, pool);
initPostController(app, pool);
initCommentController(app, pool);

app.listen(3000, () => {
    console.log("Server running on port 3000");
});
