const mysql = require("mysql2");

module.exports =  mysql.createPool({
    connectionLimit: 5,
    host: "localhost",
    user: "tritium_test",
    database: "tritium_test",
    password: "ko42Fu"
});
