const csv = require('csv-parser');
const fs = require('fs');
const chalk = require('chalk');
const dbPool= require('../db/pool');

const args = process.argv.slice(2);
const filename =args[0];

let users = [];

fs.createReadStream(filename)
    .pipe(csv({separator: ';'}))
    .on('data', (data) => {
        users.push(data);
    }).on('end', () => {
        users.forEach((user, key) => {
            dbPool.query(
                "INSERT INTO users SET" +
                " active = '" + user.ACTIVE +
                "', name = '" + user.NAME +
                "', last_name = '" + user.LAST_NAME +
                "', email = '" + user.EMAIL +
                "', xml_id = '" + user.XML_ID +
                "', gender = '" + user.PERSONAL_GENDER +
                "', birthday = '" + user.PERSONAL_BIRTHDAY +
                "', work_position = '" + user.WORK_POSITION +
                "', region = '" + user.Region +
                "', city = '" + user.City + "'",
                (err, result) => {
                    if(err) console.error(err);
                    else console.log(chalk.green("Пользователь " + user.NAME + " " + user.LAST_NAME + " успешно добавлен"));
                    if(key === (users.length - 1)) process.exit();
                }
            );

        })
});