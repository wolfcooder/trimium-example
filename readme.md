**Запуск фронтенда:**

`npm run start (в папке front)`

разворачивается на localhost:3000

**Запуск бэкенда:**

`npm run start (в папке back)`

разворачивается на localhost:4200

**Импорт пользователей из csv**

`npm run import-users csv-import/import.csv`